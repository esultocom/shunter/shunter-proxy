[![pipeline status](https://gitlab.com/esultocom/shunter/shunter-proxy/badges/main/pipeline.svg)](https://gitlab.com/esultocom/shunter/shunter-proxy/-/commits/main) [![coverage report](https://gitlab.com/esultocom/shunter/shunter-proxy/badges/main/coverage.svg)](https://gitlab.com/esultocom/shunter/shunter-proxy/-/commits/main)

# Shunter Proxy
Reverse proxy for Docker/Docker Swarm

